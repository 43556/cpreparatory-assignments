#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void display_array(int *arr);
void add_number(int *arr);
void delete_number(int *arr);
void find_max(int *arr);
void find_min(int *arr);
void sum_all(int *arr);
int main()
{
	setvbuf(stdout,NULL,_IONBF,0);
	int arr[10]={0};
    int choice;
    do{
        printf("\n\n 0. Exit \n 1. Add Number\n 2. Delete Number\n 3. Find Max\n 4. Find Min\n 5. Sum of all numbers\n 6. Show all Numbers in array\n\n Enter choice number: ");
        scanf("%d",&choice);
        switch(choice){
            case 1: add_number(arr);
            break;
            case 2: delete_number(arr);
            break;
            case 3: find_max(arr);
            break;
            case 4: find_min(arr);
            break;
            case 5: sum_all(arr);
            break;
            case 6: display_array(arr);
            break;
        }
        }while(choice!=0);
    return 0;
    }

    void display_array(int *arr){
     int i,n=10;
        for (i = 0; i < n; i++){
        printf("value= %d at index=%d\n", arr[i],i);
    }

    }
    void add_number(int *arr){
    int i,n=10;
    int pos,x;
    display_array(arr);
    printf("\nselect index to add value(0-9):");
    scanf("%d",&pos);
    printf("\nEnter Value:");
    scanf("%d",&x);
    if(pos>=0 && pos<10){
      arr[pos]=x;
    }
    else{
      printf("\n!**Enter index within [0 - 9]**!");
      exit(1);
    }
    printf("\nUpdated Array :\n");
    display_array(arr);
    }

    void delete_number(int *arr){
    int pos,i;
    display_array(arr);
    printf("select index:\n");
    scanf("%d",&pos);
    if(pos>=0 && pos<10){
        *(arr+pos)=0;
    }
    else
        printf("!**Enter index within [0 - 9]**!");
    printf("\nUpdated Array:\n");
    display_array(arr);
}

void find_max(int* arr){
    int max = 0;
    for(int i=0; i<10; i++){
        if(arr[i]>max)
            max=arr[i];
    }
    display_array(arr);
    printf("\nMaximum Value in the array is %d\n",max);
}

void find_min(int* arr){
    int min = *arr;
    for(int i=0; i<10;i++){
        if(min>arr[i])
            min = arr[i];
    }
    display_array(arr);
    printf("\nMinimum Value in the array is %d\n",min);
}

void sum_all(int* arr){
    int sum=0;
    for(int i=0; i<10; i++){
        sum += *(arr+i);
    }
    display_array(arr);
    printf("\nSum of all numbers in the array is %d\n",sum);
}
