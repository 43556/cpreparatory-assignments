#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct BankAcc
{
    int id;
    char TypeAcc[10];
    char AccHolderName[100];                             
    char Address[100];
    char ContactNum[12];

    struct BankAcc *prev;
    struct BankAcc *next;
} Node;
Node *head=NULL;
void display_forward(void);
void display_backward(void);
Node* create_node(int id, char *Type,char *accN,char *add, char* ContNum);
void add_node_at_first(int id, char *Type,char *accN,char *add, char* ContNum);
void add_node_at_last(int id, char *type,char *accN,char *add, char *ContNum);
void find_by_accid();
void find_by_accName();
void delete_all(void);
void accept_data(int *id,char *type,char *name,char *address,char *contNum);

int main()
{
         int id;
        int choice;
       // scanf("%d",&choice);
    do
    {
       
        char type[20], name[100], address[100],contNum[12];
        printf("Select any option from the following list\n");
        printf("\n\n0.Exit\n1.Add at first position\n2.Add at last position\n3.Display in forward direction\n4.Display in backward direction\n5.Find account by id\n6.Find account by name\n7.Delete all\n\nEnter choice:");
        scanf("%d",&choice);       
       
         switch(choice)
        {
        
        case 1://adding data at first position
            accept_data(&id,type,name,address,contNum);
            add_node_at_first(id,type,name,address,contNum);
            break;
        case 2://adding data at last position.
            accept_data(&id,type,name,address,contNum);
            add_node_at_last(id,type,name,address,contNum);
            break;
        case 3://display data in forward direction.
            display_forward();
            break;
        case 4://display data in backward direction
            display_backward();
            break;
        case 5://find by account id.
            find_by_accid();
            break;
        case 6://find by acccount holder name.
            find_by_accName();
            break;
        case 7://delete all.
            delete_all();
            break;

        }
    }while(choice!=0);

    return 0;
}
void accept_data(int *id,char *type,char *name,char *address,char *contNum)
{

    printf("Enter the Account Id\n");
    scanf("%d", id);

    printf("Enter the type of Account\n");
    //getchar();
    scanf("%s",type);
    printf("Enter the account holder's name\n");
    scanf("%s",name);
    printf("Enter your address\n");
    scanf("%s",address);
    printf("Enter your Mobile number\n");
    scanf("%s",contNum);

}

void delete_all()
{
    head=NULL;
}

void find_by_accName()
{
    char name[100];
    printf("enter the full name by which you want to search\n");
    getchar();
    gets(name);
    Node *trav=head;
    int count=0;
    while(trav!=NULL)
    {
        if(!strcmp(trav->AccHolderName,name))
        {
            count=1;
            break;
        }
        trav=trav->next;
    }
    if(count==1)
        printf("Record Found!!!!!\n");
    else
        printf("No matching record found!!!!\n");
}

void find_by_accid()
{
    int id;
    printf("enter the id by which you want to search\n");
    scanf("%d",&id);
    Node *trav=head;
    int count=0;
    while(trav!=NULL)
    {
        if(trav->id==id)
        {
            count=1;
            break;
        }
        trav=trav->next;
    }
    if(count==1)
        printf("Record Found!!!!!\n");
    else
        printf("No matching record found!!!!\n");
}

void display_backward()
{
    if(head!=NULL)
    {
        Node *trav=head;
        while(trav->next!=NULL)
            trav=trav->next;
        while(trav!=NULL)
        {
            printf("%d %s %s %s %s\n",trav->id,trav->TypeAcc,trav->AccHolderName,trav->Address,trav->ContactNum);
            trav=trav->prev;
        }
    }
    else
        printf("The DLL is empty!!!!\n");
}

void display_forward()
{
    if(head!=NULL)
    {
        Node *trav=head;

        while(trav!=NULL)
        {
            printf("%d %s %s %s %s\n",trav->id,trav->TypeAcc,trav->AccHolderName,trav->Address,trav->ContactNum);
            trav=trav->next;
        }
    }
    else
        printf("The DLL is empty!!!!\n");
}
Node* create_node(int id, char *type,char *accN,char *add, char *contNum)
{
    Node *temp=(Node*)malloc(sizeof(Node));
    if(temp==NULL)
    {
        printf("Malloc failed\n");
        exit(1);
    }
    temp->id= id;
    strcpy(temp->TypeAcc,type);
    strcpy(temp->AccHolderName,accN);
    strcpy(temp->Address,add);
    strcpy(temp->ContactNum,contNum);
    temp->prev=NULL;
    temp->next=NULL;
    return temp;
}


void add_node_at_first(int id, char *type,char *accN,char *add, char *ContNum)
{
    //printf("IN ADD: %d %s %s %s %s",id,type,accN,add,ContNum);
    Node *newNode=create_node(id,type,accN,add,ContNum);
    if(head==NULL)
    {
        head=newNode;
        newNode->next=NULL;
    }

    else
    {
        head->prev=newNode;
        newNode->next=head;
        newNode->prev=NULL;
        head=newNode;
    }

}
void add_node_at_last(int id, char *type,char *accN,char *add, char *ContNum)
{
    Node *newNode=create_node(id,type,accN,add,ContNum);
    if(head==NULL)
    {
        head=newNode;
        newNode->next=NULL;
    }

    else
    {
        Node *trav=head;
        while(trav->next!=NULL)
            trav=trav->next;
        trav->next=newNode;
        newNode->prev=trav;
    }
}