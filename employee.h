#ifndef _EMPLOYEE_H
#define _EMPLOYEE_H
#include "date.h"

typedef struct Employee
{
    int id;
    char name[100];
    char address[100];
    int salary;
    date_t dob;
    date_t doj;
}employee_t;

#endif