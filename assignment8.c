#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MOVIES_DB  "movies.csv"


typedef struct movies{
    int id;
    char name[100];
    char genres[200];
}movies_t;

void movie_display(movies_t *m){
    printf("id=%d name=%s genres=%s\n",m->id, m->name, m->genres);
}

typedef struct node{
    movies_t data;
    struct node *next;
}node_t;

node_t *head=NULL;  //address of the first node of the list(initially NULL).----global declaration.

node_t* create_node(movies_t val){
    node_t *newnode= (node_t*)malloc(sizeof(node_t));
    newnode->data = val;
    newnode->next = NULL;
    return newnode;

}

void add_last(movies_t val){
    node_t *newnode= create_node(val);
    if(head==NULL)
    head= newnode;
    else{
        node_t *trav= head;
        while(trav->next!= NULL){
            trav= trav->next;
        }
        trav->next= newnode;
    }

}

void display_list(){
    node_t *trav= head;
    while(trav!=NULL){
        movie_display(&trav->data);
        trav= trav->next;
    }

}


int parse_movies(char line[], movies_t *m ){
    int success=0;
    char *id, *name, *genres;
    id= strtok(line, ",\n");
    name= strtok(NULL, ",\n");
    genres= strtok(NULL, ",\n");
    if(id==NULL || name==NULL || genres==NULL)
        success=0;  //partial details
    else{
        success=1;
        m->id=atoi(id);
        strcpy(m->name, name);
        strcpy(m->genres, genres);
    }

    
    return success;

}

void load_movies(){
    FILE *fp;
    char line[1000];
    movies_t m;
    fp=fopen(MOVIES_DB, "r");
    if(fp==NULL){
        perror("Cannot open movies file.");
        exit(1);
    }
    while(fgets(line, sizeof(line), fp)!=NULL){
            //printf("%s\n",line);
            parse_movies(line, &m);
            //movie_display(&m);
            add_last(m);   //add movie into the linked list.
    }

    fclose(fp);
    

}

void find_movie_by_name(){
    char name[100];
    node_t *trav= head;
    int found= 0;
    printf("Enter movie name to be searched.\n");
    gets(name);

    trav= head;
    while(trav!=NULL){
        if(strcmp(name, trav->data.name)==0){
                movie_display(&trav->data);
            found=1;
            break;
       
        }
        trav= trav->next;
    }
    if(!found)
        printf("Movie not found.\n");
}


void find_movie_by_genre(){
    char genre[100];
    node_t *trav= head;
    int found= 0;
    printf("Enter movie genre to be searched.\n");
    gets(genre);

    trav= head;
    while(trav!=NULL){
        if(strstr(trav->data.genres, genre)!=NULL){
                movie_display(&trav->data);
            found=1;
        
        }
        trav= trav->next;
    }
    if(!found)
        printf("Movie not found.\n");
}


int main()
{
    load_movies();
    display_list(); //display movies linked list.
    find_movie_by_name();
    find_movie_by_genre();
    return 0;
}

//implementation steps
//1. read file line by line
        //a. open the file
        //b. read file line by line until eof.(use fgets().)
        //c. close the file.
//2. split each line by comma and load data into movie struct object.
//3. add movie object into linked list.
//4. find movie by name, genrefrom linked list.
//terminal> gcc assignment8.c -o assignment8.out
