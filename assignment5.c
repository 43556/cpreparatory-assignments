#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "employee.h"
#include "date.h"

int main()
{
        int diff,age,exp; 
        employee_t e;
        printf("\nEnter Employee details:\n\n");
        printf("\nID: ");
        scanf("%d",&e.id);
        printf("\nname: ");
        scanf("%s",e.name);
        printf("\nAddress: ");
        scanf("%s",e.address);
        printf("\nSalary: ");
        scanf("%d",&e.salary);
        printf("\nDate Of Birth: ");
        date_accept(&e.dob);
        printf("\nDate of Joining: ");
        date_accept(&e.doj);

        diff=date_cmp(date_current(),e.dob);
        age=diff/365;
        printf("\nAge of the Employee= %d years or %d days\n",age,diff);
        exp=date_cmp(date_current(),e.doj);
        printf("\nExperience of the Employee=%d days\n",exp);
        if(exp>90)
        printf("\nProvibition period is over\n\n");
        else
        printf("\nprovibition period is still on\n\n");
        return 0;
}