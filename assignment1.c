#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main()
{
    char str[] = "Welcome,to,Sunbeam,CDAC,Diploma,Course";

    // Returns first token
    char* token = strtok(str, ",");

    // Keep printing tokens while one of the
    // delimiters present in str[].
    while (token != NULL) 
    {
        printf("%s\n", token);
        token = strtok(NULL, ",");
    }

    return 0;
}