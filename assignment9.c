#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define ITEM_DB     "items.db"
#define TEMP_DB     "temp.db"
typedef struct item
{
    int id;
    char name[80];
    double price;
    int quantity;
}item_t;



 void accept_item(item_t *i)
 {   
     printf("Enter the id         : ");
     scanf("%d",&i->id);
     printf("Enter the Name       : ");
     scanf("%s",i->name);
     printf("Enter the Price      : ");
     scanf("%lf",&i->price);
     printf("Enter the Quantity   : ");
     scanf("%d",&i->quantity);
     
 }

 void print_item(item_t *i)
 {
     printf("ID : %d , PRODUCT : %-10s ,  PRICE :  %5lf ,  QUANTITY   :  %5d\n",i->id,i->name,i->price,i->quantity);
 }

void add_item(item_t *i)
{

	FILE *fp;
	fp = fopen(ITEM_DB,"ab");
	if(fp == NULL)
	{
		perror("failed to open");
		return;
	}
	//write user data into file
	fwrite( i , sizeof(item_t) , 1 , fp );		
	printf("Product added into file.\n");

	fclose(fp);

}


void item_find_by_name(char name[])
{
    FILE *fp;
    int found = 0;
    item_t i;

    fp = fopen(ITEM_DB , "rb");
    if(fp == NULL)
    {
        perror("cannot open file.\n");
        return;
    }
    while(fread(&i ,sizeof(item_t),1,fp) > 0)
    {
        if (strstr(i.name , name) != NULL)
        {
            found = 1;
            print_item(&i);
        }
    }

    fclose(fp);
    if(!found)
    {
        printf("No such item found");
    }
}



void display_all_products()
{
    FILE *fp;
    item_t i;
    
    fp = fopen(ITEM_DB ,"rb");
    if (fp == NULL)
    {
        perror("cannot open file.\n");
        return;
    }

    while(fread( &i , sizeof(item_t), 1 ,fp) > 0)
    {
        print_item(&i);
    }
    fclose(fp);
}


void product_edit_by_id()
{
    int found = 0, id;
    FILE *fp;
    item_t i;

    printf("Enter the product id    : ");
    scanf("%d",&id);

    fp = fopen(ITEM_DB ,"rb+");
    if (fp == NULL)
    {
        perror("cannot open file.\n");
        return;
    }
    while(fread(&i,sizeof(item_t),1,fp) > 0)
    {
        if(id == i.id)
        {
            found = 1;
            break;
        }
    }

    if(found)
    {
        long size = sizeof(item_t);
        item_t ni;
        accept_item(&ni);
        ni.id = i.id;

        fseek(fp , -size , SEEK_CUR);

        fwrite(&ni , size , 1 , fp);

        printf("Product details are updated.\n");
    }
    else
    {
        printf("Product not found.\n");
    }
    

    fclose(fp);
}



void delete_item()
{
    int id, found = 0;
    item_t u;
    FILE *fp;
    FILE *fp_tmp;
    printf("Enter item ID : "); 
    scanf("%d",&id);    
    fp=fopen(ITEM_DB,"rb");
    fp_tmp=fopen(TEMP_DB,"wb");
    if(fp==NULL)
    {
        perror("failed to open items file");
        return;
    }

    if(fp_tmp==NULL)
    {
        perror("failed to open temp file");
        return;
    }
    //read items 1 by 1
    while(fread(&u,sizeof(item_t),1,fp)>0)
    {
        // book found diplay book
        if(id==u.id)
        {
        found = 1;
        printf("\nItem %d is deleted from inventory   :   ",u.id);      
        }
        else
        {
            fwrite(&u, sizeof(item_t),1,fp_tmp);
        }
    }
    if(!found)
    {
        printf("\n No such item found \n");
    }
                   
    fclose(fp);// close file
    fclose(fp_tmp);
    remove(ITEM_DB);
    rename(TEMP_DB,ITEM_DB);
}

void add()
{
    item_t i;
    accept_item(&i);
    print_item(&i);
    add_item(&i);
}



int main(void)
{
    int choice;
    char name[80];

    do
    {
        
        printf("\n\n0. Exit\n1. Add\n2. Find\n3. Display\n4. Edit\n5. Delete\n\nEnter your choice   : ");
        scanf("%d",&choice);

        switch(choice)
        {
            case 1:
               
                add();
                break;

            case 2:
                //Find();

                printf("Enter the name of product   : ");
                scanf("%s",name);
                item_find_by_name(name);
                break;

            case 3:
                //Display();
                display_all_products();
                break;

            case 4:
               // Edit();
               product_edit_by_id();
                break;

            case 5:
                //Delete();
                delete_item();
                
                break;
        }
    }while(choice != 0);



    return 0;
}
