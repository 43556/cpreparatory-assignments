#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct subject{
        char subname[30];
        int marks;
    }sub_t;
typedef struct student {
	int roll;
	char name[30];
	int std;
    sub_t s1;
    sub_t s2;
    sub_t s3;
	sub_t s4;
    sub_t s5;
    sub_t s6;
}student_t;

void accept_student(student_t *s) {
	printf("roll: ");
	scanf("%d", &s->roll);
	printf("name: ");
	scanf("%s", s->name);
	printf("std: ");
	scanf("%d", &s->std);
	printf("1st Subject Name: ");
	scanf("%s", s->s1.subname);
    printf("Marks : ");
	scanf("%d", &s->s1.marks);
    printf("2nd Subject Name: ");
	scanf("%s", s->s2.subname);
    printf("Marks : ");
	scanf("%d", &s->s2.marks);
    printf("3rd Subject Name: ");
	scanf("%s", s->s3.subname);
    printf("Marks : ");
	scanf("%d", &s->s3.marks);
    printf("4th Subject Name: ");
	scanf("%s", s->s4.subname);
    printf("Marks : ");
	scanf("%d", &s->s4.marks);
    printf("5th Subject Name: ");
	scanf("%s", s->s5.subname);
    printf("Marks : ");
	scanf("%d", &s->s5.marks);
    printf("6th Subject Name: ");
	scanf("%s", s->s6.subname);
    printf("Marks : ");
	scanf("%d", &s->s6.marks);
}
void display_student(student_t *s) {
	printf("Roll NO: %d\tName: %s\tStd: %d\n1st Subject: %s\tMarks: %d\n2nd Subject: %s\tMarks: %d\n3rd Subject: %s\tMarks: %d\n4th Subject: %s\tMarks: %d\n5th Subject: %s\tMarks: %d\n6th Subject: %s\tMarks: %d\n\n", s->roll, s->name, s->std, s->s1.subname, s->s1.marks, s->s2.subname, s->s2.marks, s->s3.subname, s->s3.marks, s->s4.subname, s->s4.marks, s->s5.subname, s->s5.marks, s->s6.subname, s->s6.marks);
}

#define SIZE	5

typedef struct cirque {
	student_t arr[SIZE];
	int rear;
	int front;
	int count;
}cirque_t;

void cq_init(cirque_t *q) {
	memset(q->arr, 0, sizeof(q->arr));
	q->front = -1;
	q->rear = -1;
	q->count = 0;
}

void cq_push(cirque_t *q, student_t s) {
	q->rear = (q->rear+1) % SIZE;
	q->arr[q->rear] = s;
	q->count++;
}

void cq_pop(cirque_t *q) {
	q->front = (q->front+1) % SIZE;	
	q->count--;
}

student_t cq_peek(cirque_t *q) {
	int index = (q->front+1) % SIZE;
	return q->arr[index];
}

int cq_empty(cirque_t *q) {
	return q->count == 0;
}

int cq_full(cirque_t *q) {
	return q->count == SIZE;
}

int main() {
	cirque_t q;
	student_t temp;
	int choice;
	cq_init(&q);
	do {
		printf("\n0. exit\n1. push\n2. pop\n3. peek\nenter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // push
			if(cq_full(&q))
				printf("queue is full.\n");
			else {
				accept_student(&temp);
				cq_push(&q, temp);
			}
			break;
		case 2: // pop
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				cq_pop(&q);
				printf("popped: ");
				display_student(&temp);
			}
			break;
		case 3: // peek
			if(cq_empty(&q))
				printf("queue is empty.\n");
			else {
				temp = cq_peek(&q);
				printf("topmost: ");
				display_student(&temp);
			}
			break;
		}
	} while(choice != 0);
	return 0;
}
